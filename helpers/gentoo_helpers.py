from portage.versions import *
from os import path, scandir
import itertools

def get_highest_version(packages: list[tuple]) -> str:
    '''
    Return the highest version in a list of pkgsplit (pn, ver, rev) tuples of arbitrary length
    (for the same package)
    '''
    # Grab the first element of the input list to init; it's fast and
    # itertools.combinations will be skipped if < 2 packages
    highest_ver = packages[0]
    for x, y in itertools.combinations(packages,2):
        highest = pkgcmp(x,y)
        if highest == 1 and pkgcmp(x,highest_ver[-1]) == 1:
            highest_ver = x
        elif highest == -1 and pkgcmp(y,highest_ver[-1]) == 1:
            highest_ver = y
    return highest_ver[1]

def parse_gentoo_repository(loc: str = "/var/db/repos/gentoo") -> list[tuple]:
    '''
    Parse a gentoo ebuild repository and return the get the latest version of each package as a tuple
    '''
    packages: list[tuple] = []
    notcategories: list = [
        'acct-user',
        'acct-group',
        'app-alternatives',
        'eclass',
        'metadata',
        'profiles',
        'scripts',
        'virtual',
    ]
    # Grab a list of category names
    with scandir(loc) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_dir() and entry.name not in notcategories:
            # Iterate over categories and packages to get the info we need
                category = entry.name
                with scandir(path.join(loc,category)) as it:
                    for entry in it:
                        if not entry.name.startswith('.') and entry.is_dir():
                            ebuilds = []
                            with scandir(entry.path) as it:
                                for entry in it:
                                    # TODO: Edge case: only live ebuild (*9999*) will not record the ebuild
                                    if entry.is_file() and entry.name.endswith('.ebuild') and '9999' not in entry.name:
                                        ebuilds.append(pkgsplit(f'{entry.name.replace(".ebuild","")}'))
                            # Could do better here if I was willing to deal with package dupes
                            if len(ebuilds) > 0:
                                packages.append((category, ebuilds[0][0], get_highest_version(ebuilds)))
    return packages
