from git import RemoteProgress, Repo
from os import path

class GitProgressPrinter(RemoteProgress):
    def update(self, op_code, cur_count, max_count=None, message=""):
        print(
            op_code,
            cur_count,
            max_count,
            cur_count / (max_count or 100.0),
            message or "NO MESSAGE",
        )

# Pull a git repo at path
def update_repo(repopath: str):
    repo = Repo(path.abspath(repopath))
    # We want a real, clean repo
    assert not repo.bare and not repo.is_dirty()
    origin = repo.remote()
    assert origin.exists()
    print(f'Updating {repo.active_branch} from {origin}')
    for fetch_info in origin.fetch(progress=GitProgressPrinter()):
        print(f'Updated {fetch_info.ref} to {fetch_info.commit}')
