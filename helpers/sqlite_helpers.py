from portage.versions import *
import csv
import re
import sqlite3

def execute_transaction(database: str, table: str, query: str):
    '''
    Open a connection to our database, execute our query in a transaction
    and output some useful logging information, then neatly close the connection
    '''
    try:
        connection = sqlite3.connect(database)
        cursor = connection.cursor()
        print(f'Connected to {database}')
        cursor.execute(query)
        connection.commit()
        verb: str = ''
        if 'INSERT' in query:
            print(f'Inserted {cursor.rowcount} rows into {table}')
        elif 'UPDATE' in query:
            print(f'Updated {cursor.rowcount} rows in {table}')
        elif 'CREATE' in query:
            print(f'Created {table}')
        cursor.close()

    except sqlite3.Error as error:
        f = open(f'./{table}query.sql',"w")
        f.write(query)
        f.close
        raise Exception(f'Failed to execute query on {table}', error)
    finally:
        if connection:
            connection.close()
            print(f'Closed connection to {database}')

def fedora_package_sql(packages: list, database):
    '''
    Populate the fedora_packages, fedora_uri and fedora_upstreamuri tables
    '''
    # Do the packages first so that we can query the id of packagename from sqlite
    # fedora_uri and fedora_upstreamuri can be actioned as a single transaction
    fedorapackages: list[tuple] = []
    for package in packages:
        notes = package["notes"] if "notes" in package else "NULL"
        fedorapackages.append((package["name"], notes))
    packagestatement = generate_sqlite_insert_statement('fedora_packages', ('packagename','patchtext'), fedorapackages)
    execute_transaction(database, 'fedora_packages', packagestatement)
    # query fedora_packages and retrieve ids to generate foreign keys for remaining tables
    dbpackages: dict = {}
    for row in retrieve_all_rows(database, 'fedora_packages'):
        dbpackages[row[0]] = row[1]
    fedoraurirows: list[tuple] = []
    upstreamurirows: list[tuple] = []
    for package in packages:
        # This is an array that needs to be parsed!
        for uri in package["fedorauris"]:
            fedoraurirows.append((uri,dbpackages[package["name"]]))
        for uri in package["upstreamuris"]:
            # This is a tuple (uri, uritext)
            upstreamurirows.append((uri[0],uri[1],dbpackages[package["name"]]))
    execute_transaction(database, 'fedora_uris',
        generate_sqlite_insert_statement('fedora_uris',("uri","packageid"), fedoraurirows))
    execute_transaction(
        database, 'fedora_upstreamuris',
        generate_sqlite_insert_statement('fedora_upstreamuris',("uri","uritext","packageid"), upstreamurirows))

def generate_sqlite_insert_statement(tablename: str, columns: tuple, rows: list[tuple]) -> str:
    '''
    Generate a SQLite insert statement for inputs:
    table_name, ('columnx', ... 'columny'), [(column_content,...)]
    '''
    insert: list[str] = []
    insert.append(f'INSERT INTO {tablename} ({",".join(columns)})')
    insert.append("VALUES")
    for idx, row in enumerate(rows):
        eol = ";" if idx == len(rows)-1 else ','
        content: list[str] = []
        # Turn our values into something that SQL will like!
        for value in row:
            # No quotes
            if type(value) == 'int':
                content.append(value)
            elif type(value) == 'str' and value.isnumeric():
                content.append(value)
            elif value == "NULL":
                content.append("NULL")
            else:
            # Quotes!
                content.append(f"\"{value}\"")
        insert.append(f"\t({','.join(content)}){eol}")
    return '\n'.join(insert)

def sqlite_init_tables(database):
    '''
    Initialise tables
    '''
    tables = [
        { 'name': 'gentoo_packages',
            'columns': [
                { 'name': 'category','type': 'TEXT' },
                { 'name': 'id', 'type': 'INTEGER', 'primary_key': True},
                { 'name': 'package', 'type': 'TEXT'},
                { 'name': 'version', 'type': 'TEXT'},
            ]
        },
        { 'name': 'fedora_packages',
            'columns': [
                { 'name': 'packagename', 'type': 'TEXT', 'unique': True},
                { 'name': 'id', 'type': 'INTEGER','primary_key': True},
                { 'name': 'patchtext', 'type': 'TEXT'},
            ]
        },
        { 'name': 'fedora_upstreamuris',
            'columns': [
                { 'name': 'id', 'type': 'INTEGER','primary_key': True},
                { 'name': 'uri', 'type': 'TEXT'},
                { 'name': 'uritext', 'type': 'TEXT'},
                { 'name': 'packageid', 'type': 'INTEGER'},
            ],
            'constraints': {
                'foreign_key': {
                        'name': 'packageid',
                        'table': 'fedora_packages',
                        'references': 'id',
                }
            }
        },
        { 'name': 'fedora_uris',
            'columns': [
                { 'name': 'id', 'type': 'INTEGER','primary_key': True},
                { 'name': 'uri', 'type': 'TEXT'},
                { 'name': 'packageid', 'type': 'INTEGER'},
            ],
            'constraints': {
                'foreign_key': {
                        'name': 'packageid',
                        'table': 'fedora_packages',
                        'references': 'id',
                }
            }
        },
        { 'name': 'package_translations',
            'columns': [
                { 'name': 'fedorapackage', 'type': 'TEXT', 'unique': True},
                { 'name': 'gentoocategory', 'type': 'TEXT'},
                { 'name': 'gentoopackage', 'type': 'TEXT'},
                { 'name': 'id', 'type': 'INTEGER', 'primary_key': True},
            ]
        },
        { 'name': 'gentoo_bugs',
            'columns': [
                { 'name': 'id', 'type': 'INTEGER', 'primary_key': True},
                { 'name': 'status', 'type': 'INTEGER'},
                { 'name': 'summary', 'type': 'INTEGER'},
                { 'name': 'packageid', 'type': 'INTEGER'},
            ],
            'constraints': {
                'foreign_key': {
                        'name': 'packageid',
                        'table': 'gentoo_packages',
                        'references': 'id',
                }
            }
        }
    ]
    for table in tables:
        statement: list = []
        statement.append(f'CREATE TABLE {table["name"]} (\n')
        for column in table["columns"]:
            # column_name data_type [PRIMARY KEY, NOT NULL, DEFAULT 0, etc.]
            statement.append(f'\t{column["name"]} {column["type"]}')
            if "primary_key" in column:
                statement.append(' PRIMARY KEY,\n')
            elif "unique" in column:
                statement.append(' UNIQUE,\n')
            else:
                statement.append(',\n')
        if "constraints" in table:
            # There are other constraints, but this is all I care about for now
            if "foreign_key" in table["constraints"]:
                statement.append(f'\tFOREIGN KEY ({table["constraints"]["foreign_key"]["name"]})\n')
                statement.append(f'\t\tREFERENCES {table["constraints"]["foreign_key"]["table"]} ({table["constraints"]["foreign_key"]["references"]})\n')
        # Tidy up the last line of the statement
        lastrow = statement[-1].replace(',\n','\n')
        del statement[-1]
        statement.append(lastrow)
        statement.append(");")
        execute_transaction(database, table["name"], ''.join(statement))

def retrieve_all_rows(database: str, table: str) -> list:
    '''
    Retrieve all rows from arbitrary table of a database
    '''
    try:
        connection = sqlite3.connect(database)
        cursor = connection.cursor()
        print(f'Connected to {database}')
        cursor.execute(f'SELECT * from {table}')
        records = cursor.fetchall()
        print(f'Retrieved {len(records)} rows from {table}')
        cursor.close()

    except sqlite3.Error as error:
        print(f'Failed to read data from {table}', error)
    finally:
        if connection:
            connection.close()
            print(f'Closed connection to {database}')

    return records

def write_gentoo_bugs(database, bugs):
    '''
    Take information from a list of gentoo bugs, match with packages in DB,
    then write it to the gentoo_bugs table of `database`
    '''
    # A list of last-rited package bugs; they have been removed from ::gentoo so don't parse (and we don't care)
    lastrited = [
        870730, 870736, 871057, 871108, 871216,
        871405, 874003, 874486, 874660, 874867,
        874933, 875053, 875053, 875164, 875254,
        875278, 875464, 875668, 875686, 878609,
        879637, 880513, 880845, 880927, 882089,
        882263
    ]
    # The bug number makes a fine primary key, so let's use that
    # Make packages a dict for easy and quick access
    # packagedict[category][package] = id
    packagedict: dict = {}
    # This should always return [(id,category,package,version),...]
    for row in retrieve_all_rows(database, 'gentoo_packages'):
        if row[0] not in packagedict:
            packagedict[row[0]] = {}
        packagedict[row[0]][row[2]] = row[1]

    # try and find cpv at the beginning of a bug
    pkgre = re.compile(r"\s?(?P<cpv>[\w-]*?/[\w\.+-]*?)[: ,]")
    gentoobugs: list[tuple] = []
    gurubugs: int = 0
    for bug in bugs:
        if bug["id"] in lastrited:
            continue
        if '[guru]' in bug["summary"].lower():
            # Guru is out of scope (though it would be trivial to re-use the gentoo code and add a special case)
            gurubugs += 1
            continue
        summarymatch = pkgre.search(bug["summary"])
        package = catpkgsplit(summarymatch.group('cpv'))
        if package is None:
            package = catsplit(summarymatch.group('cpv'))
        id = packagedict[package[0]][package[1]]
        gentoobugs.append((bug["id"], bug["status"], bug["summary"].replace('"','""'), id))
    print(f'There are {gurubugs} additional bugs relating to GURU that were not processed')
    execute_transaction(database, 'gentoo_bugs',
        generate_sqlite_insert_statement('gentoo_bugs', ('id', 'status', 'summary', 'packageid'), gentoobugs))

def write_mapping_table(database, mapping_csv: str = "./mapping.csv"):
    '''
    Read a mapping table from csv and write it to the sqlite db
    '''
    map: list[tuple] = []
    with open(mapping_csv, 'r') as file:
        mapping = csv.DictReader(file)
        for row in mapping:
            if row != ['Fedora Package', 'Gentoo Category', 'Gentoo Package']:
                map.append((row["Fedora Package"], row["Gentoo Category"], row["Gentoo Package"]))

    # This may be empty, so skip the DB transaction
    if len(map) > 0:
        execute_transaction(database, 'package_translations',
            generate_sqlite_insert_statement('package_translations',('fedorapackage', 'gentoocategory', 'gentoopackage'), map))
