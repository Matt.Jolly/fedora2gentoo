#! /usr/bin/env python

# Convert fedora-modernc repo to gentoo ebuilds to generate a hitlist
# ideally this will update a sqlite database that can take updates until hitlist is done

# read modernc repo (git pull first!)

# read ::gentoo repo (also git pull first!)

from portage.versions import *
from helpers.sqlite_helpers import *
from helpers.git_helpers import *
from helpers.gentoo_helpers import *
import bugzilla
import os
import regex as re
import pickle

def parse_fedora_modernc(loc: str) -> list[dict]:
    '''
    Parse a fedora modernc repository at location and extract useful information
    '''
    packages: list[dict] = []
    # structure in repo ./pkg/[a-z]/pkgname.md
    mdlinkregex = re.compile(r"(?:\[(?P<text>.*?)\])\((?P<uri>.*?)\)")
    with os.scandir(os.path.join(loc,'pkg')) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_dir():
                # Iterate over a-z directories and parse each md file to get the info we need
                with os.scandir(entry.path) as it:
                    for entry in it:
                        package = {
                            'fedorauris': [],
                            'upstreamuris': [],
                        }
                        notes = []
                        if entry.is_file() and '.md' in entry.name:
                            package['name'] = entry.name.replace('.md','')
                            with open(entry.path, 'r') as moderncmd:
                                for line in moderncmd:
                                    # if line contains this branch it's going to be a fedora RPM commit; just strip the list marker
                                    # TODO, maybe: Like two rawhide URIs include comments. One is a Markdown Link.
                                    if 'branch=rawhide' in line or 'bugzilla.redhat.com' in line:
                                        package['fedorauris'].append(line.replace('* ',''))
                                    # Parse any links in list items
                                    elif '* ' in line and re.search(mdlinkregex,line):
                                        # quick and dirty regex for any upstream URIs
                                        match = re.search(mdlinkregex,line)
                                        package['upstreamuris'].append((match.group('uri'), match.group('text').replace('"','""')))
                                    else:
                                        # It's flavour text!
                                        if line.strip():
                                            notes.append(line)
                            # No need to add flavour text if there isn't any
                            if len(notes):
                                package['notes'] = ''.join(notes)
                            packages.append(package)
    return packages

def query_gentoo_tracker_bug(bugnumber: int) -> list[dict]:
    url = "bugs.gentoo.org"
    keyfile = open(os.path.abspath('./bugzilla_api_key'))
    api_key = keyfile.read().replace('\n','')
    buglist: list[dict] = []
    print('connecting to b.g.o')
    bzapi = bugzilla.Bugzilla(url, api_key)
    trackerbug = bzapi.getbug(bugnumber)
    print(f'Tracker Bug: {trackerbug}')
    print(f'Querying each blocker for it status... this may take a while!')
    for dependsbug in trackerbug.depends_on:
        thisbug = bzapi.getbug(dependsbug)
        bug = {
            "id": thisbug.id,
            "summary": thisbug.summary,
            "status": thisbug.status
        }
        buglist.append(bug)
    return buglist

def main():
    # Implement some CLI stuff eventually
    database = os.path.abspath("./fedora2gentoo.sqlite3")
    # I don't have perms setup for /var/db/repos/gentoo, this is easier!
    gentoorepo = '/data/development/temp/modernc/gentoo'
    # This is florian's fedora-modernc repo; see readme
    moderncrepo = '/data/development/temp/modernc/fedora-modernc'
    gentootrackerbug = 870412

    # Purge the DB between runs until we can conditionally initialise
    os.remove(database)
    # Ensure that our datasources are up to date
    update_repo(gentoorepo)
    update_repo(moderncrepo)
    # Initialise our tables todo: Make optional
    sqlite_init_tables(database)
    # Parse ::gentoo and write it to the database
    execute_transaction(database, 'gentoo_packages',
        generate_sqlite_insert_statement('gentoo_packages',
            ('category','package','version'),parse_gentoo_repository(gentoorepo)))
    moderncpackages: list = parse_fedora_modernc(moderncrepo)
    fedora_package_sql(moderncpackages, database)
    write_mapping_table(database)

    bugs: list[dict] = []
    # See if we have any cached bugs so that we don't smash b.g.o
    if os.path.isfile('./savedbugs'):
        print('Loading saved bug list!')
        with open('./savedbugs', 'rb') as file:
            bugs = pickle.load(file)
            print(type(bugs))
    else:
        print('Querying bugzilla!')
        bugs = query_gentoo_tracker_bug(gentootrackerbug)
        with open('./savedbugs', 'wb') as file:
            pickle.dump(bugs, file, protocol=pickle.HIGHEST_PROTOCOL)
    write_gentoo_bugs(database, bugs)
    print('Database should be good to go!')

if __name__ == "__main__":
    main()
