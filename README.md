Compare fedora-modernc repo to Gentoo ebuilds so that we can avoid duplication!

This currently just rebuilds the DB every time, it could really stand to handle updates, but querying datasources is cheap.

You will need to provide a gentoo bugzilla api key (./bugzilla_api_key). The results of our query are stored locally to avoid load on the Gentoo infra

Until I can be bothered adding a real interface to the script you'll need to modify the fedora-modernc and gentoo git repo locations.
# sources

- `::gentoo` - must be git repo. The script will update this for you; make sure you can write to the location - I don't use /var/db/repos/gentoo!
- `fedora-modernc` git repo (https://gitlab.com/fweimer-rh/fedora-modernc)
- Gentoo bugzilla - bugs related to https://bugs.gentoo.org/870412
- `./mapping.csv` - This is manually generated. As florian adds new packages it'll need to be updated
    - Packages not in mapping do not exist in ::gentoo
    - A couple of vlookups can do most of the work

# Dependencies:

dev-python/GitPython
dev-python/python-bugzilla

# instructions

```
echo bugzilla_api_key > ./bugzilla_api_key
./fedora2gentoo.py
```

you might need to touch the sqlite3 DB, somehow that's getting deleted at the wrong time.